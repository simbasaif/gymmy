//
//  gymmyApp.swift
//  gymmy
//
//  Created by Saif Azim on 2/20/23.
//

import SwiftUI

@main
struct gymmyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
